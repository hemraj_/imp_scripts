#!/usr/bin/env bash
#update
yum -y update
yum -y install epel-release
# Install apache
echo '############## Installing apache server ##################'
yum -y install httpd
chkconfig --levels 235 httpd on
/etc/init.d/httpd start
# Install php 5.6
echo '############## Installing php 5.6##################'
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
wget https://centos6.iuscommunity.org/ius-release.rpm
rpm -Uvh ius-release*.rpm
yum -y update
yum -y install php56u php56u-opcache php56u-xml php56u-mcrypt php56u-gd php56u-devel php56u-mysql php56u-intl php56u-mbstring php56u-bcmath php56u-soap
/etc/init.d/httpd restart
echo '############## mysql 5.6 ##################'
wget http://repo.mysql.com/mysql-community-release-el6-5.noarch.rpm
rpm -Uvh mysql-community-release-el6-5.noarch.rpm
yum -y install mysql mysql-server
chkconfig mysqld on
service mysqld start
mysqladmin -u root password root
echo '############## Installing phpadmin ##################'
yum -y install epel-release phpmyadmin rpm-build redhat-rpm-config
yum -y update
yum install phpmyadmin
sed 's/Allow from None/Allow from All/g' /etc/httpd/conf.d/phpMyAdmin.conf

#set root password
echo '############## Root password reset ##################'
yum -y install passwd
echo 'Setting root password to 123456'
echo -e "123456\n123456" | passwd root

echo '############## Installing ssh server ##################'
yum install -y openssh-server
chkconfig sshd --add
chkconfig sshd on --level 2 3 4 5
service sshd start
echo 'Installation completed \nOS Root Password is 123456 and Database Password is root'
